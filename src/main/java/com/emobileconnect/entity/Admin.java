package com.emobileconnect.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "admin")
public class Admin {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer adminId;
	private String adminName;
	private String adminDescription;
	private String phoneNumber;
	
	
	public Admin() {
		
	}
	
	public Admin(Integer adminId, String adminName, String adminDescription, String phoneNumber) {
		super();
		this.adminId = adminId;
		this.adminName = adminName;
		this.adminDescription = adminDescription;
		this.phoneNumber = phoneNumber;
	}
	
	public Integer getAdminId() {
		return adminId;
	}
	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public String getAdminDescription() {
		return adminDescription;
	}
	public void setAdminDescription(String adminDescription) {
		this.adminDescription = adminDescription;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
}
