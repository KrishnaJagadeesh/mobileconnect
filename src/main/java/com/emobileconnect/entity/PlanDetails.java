package com.emobileconnect.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "plandetails")
public class PlanDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer planId;
	private String planName;
	private Double planAmount;
	private String planDescription;
	
	public PlanDetails(){
		
	}
	
	public PlanDetails(Integer planId, String planName, Double planAmount, String planDescription) {
		super();
		this.planId = planId;
		this.planName = planName;
		this.planAmount = planAmount;
		this.planDescription = planDescription;
	}
	
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Double getPlanAmount() {
		return planAmount;
	}
	public void setPlanAmount(Double planAmount) {
		this.planAmount = planAmount;
	}
	public String getPlanDescription() {
		return planDescription;
	}
	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}
	
	
	
}
