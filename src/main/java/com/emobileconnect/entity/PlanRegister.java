package com.emobileconnect.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "planregister")
public class PlanRegister {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer planRegisterId;
	private Integer userId;
	private String connectionStatus;
	private Integer planId;
	
	public PlanRegister() {
		
	}
	
	public PlanRegister(Integer planRegisterId, Integer userId, String connectionStatus, Integer planId) {
		super();
		this.planRegisterId = planRegisterId;
		this.userId = userId;
		this.connectionStatus = connectionStatus;
		this.planId = planId;
	}
	
	public Integer getPlanRegisterId() {
		return planRegisterId;
	}
	public void setPlanRegisterId(Integer planRegisterId) {
		this.planRegisterId = planRegisterId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getConnectionStatus() {
		return connectionStatus;
	}
	public void setConnectionStatus(String connectionStatus) {
		this.connectionStatus = connectionStatus;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	
}
