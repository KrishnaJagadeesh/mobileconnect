package com.emobileconnect.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer userId;
	private String userName;
	private String email;
	private String existingPhoneNumber;
	private String address;
	private String adharNumber;
	
	public User() {
		
	}
	
	public User(Integer userId, String userName, String email, String existingPhoneNumber, String address,
			String adharNumber) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.email = email;
		this.existingPhoneNumber = existingPhoneNumber;
		this.address = address;
		this.adharNumber = adharNumber;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getExistingPhoneNumber() {
		return existingPhoneNumber;
	}
	public void setExistingPhoneNumber(String existingPhoneNumber) {
		this.existingPhoneNumber = existingPhoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAdharNumber() {
		return adharNumber;
	}
	public void setAdharNumber(String adharNumber) {
		this.adharNumber = adharNumber;
	}
	
}
