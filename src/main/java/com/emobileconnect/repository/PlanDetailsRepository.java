package com.emobileconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emobileconnect.entity.PlanDetails;

@Repository
public interface PlanDetailsRepository extends JpaRepository<PlanDetails, Integer>{

}
