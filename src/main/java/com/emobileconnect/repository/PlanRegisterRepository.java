package com.emobileconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.emobileconnect.entity.PlanRegister;

public interface PlanRegisterRepository extends JpaRepository<PlanRegister, Integer> {

}
